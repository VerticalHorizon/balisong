class Canvas {
	constructor(id) {
		this.element = document.getElementById(id)
		this.context = this.element.getContext("2d");
	}

	onClick(fn) {
		this.element.addEventListener("click", fn);
	}

	clear() {
		this.context.clearRect(0, 0, this.element.width, this.element.height);
	}

	drawPath(path) {
		this.context.beginPath()
		let start = path[path.length-1]
		this.context.moveTo(start[0], start[1]);

		for (let i = path.length - 2; i >= 0; i--) {
			this.context.lineTo(path[i][0], path[i][1]);
		}

		this.context.stroke();
	}

	reflect(path, x, y) {
		let new_path = []
		for (let i = path.length - 1; i >= 0; i--) {
			new_path.push([x ? 900 - path[i][0] : path[i][0], y ? 800 - path[i][1] : path[i][1]])
		}
		return new_path
	}

	shift(path, x=0, y=0) {
		let new_path = []
		for (let i = path.length - 1; i >= 0; i--) {
			new_path.unshift([y + path[i][0], x + path[i][1]])
		}
		return new_path
	}
}


path = [
	// x, y
	[100, 600],	//
	[110, 590],
	[120, 590],
	[130, 590],
	[140, 590],
	[150, 590],
	[160, 590],
	[170, 590],
	[180, 590],
	[190, 590],
	[200, 590],
	[780, 590],

	[800, 600],	//
	[800, 500],	//
	[100, 500],	//
	[100, 600],	//
]
for (var i = 0; i < path.length; i++) {
	let opt = document.createElement("option")
	let textnode = document.createTextNode(path[i].join(", "));
	opt.appendChild(textnode);
	opt.setAttribute("data-id", i);
	opt.addEventListener("click", function(){console.log(this.getAttribute("data-id"))})
	document.getElementById("path").appendChild(opt);
}

let x = new Canvas("balisongX")
let z = new Canvas("balisongZ")
x.onClick(function (e) {
	let p = document.getElementById("path")
	let selectedPoint = p.options[p.selectedIndex].getAttribute("data-id")
	p.options[p.selectedIndex].innerText = [e.offsetX, e.offsetY].join(", ")

	path[selectedPoint] = [e.offsetX, e.offsetY]
	build(x, z)
})

build(x, z)

function build(x, z) {
	x.clear()
	z.clear()

	x.drawPath(path)
	let upside_down = x.shift(x.reflect(path, false, true), y=190)
	x.drawPath(upside_down)

	z.drawPath(x.shift(x.reflect(path, true, false), y=-110))
	z.drawPath(x.shift(x.reflect(upside_down, true, false), y=110))
}